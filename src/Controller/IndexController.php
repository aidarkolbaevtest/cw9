<?php

namespace App\Controller;

use App\Entity\Note;
use App\Entity\User;
use App\Form\SignInType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Note\NoteHandler;
use App\Model\User\UserHandler;
use App\Repository\NoteRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return Response
     */
    public function indexAction(Request $request, ApiContext $apiContext)
    {
        $phrase = $request->get('phrase') ?: 'cat';
        $sort = $request->get('sort') ?: 'new';
        $limit = $request->get('limit') ?: 10;
        try {
            $reddit_response = $apiContext->getPostsByParameters($phrase, $sort, $limit);
            foreach ($reddit_response['data']['children'] as $post) {
                $response[] = $post['data'];
            }
            if (!$reddit_response) {
                return $this->redirectToRoute('homepage');
            }
        } catch (ApiException $e) {
            $error = 'Что-то пошло не так';
        }
        return $this->render('index/index.html.twig', [
            'error' => $error ?? null,
            'posts' => $response ?? []
        ]);
    }


    /**
     * @Route("/details/{name}", name="details")
     * @param string $name
     * @param ApiContext $apiContext
     * @param NoteRepository $noteRepository
     * @return Response
     */
    public function detailsAction(string $name, ApiContext $apiContext, NoteRepository $noteRepository) {
        try {
            $reddit_response = $apiContext->getPostByName($name);
            $response = $reddit_response['data']['children'][0]['data'];
            if($note = $noteRepository->checkNoteExists($response['name'])) {
                if($noteRepository->isAlreadyFavorite($this->getUser(), $note)) {
                    $already_favorite = true;
                };
                $fav_amount = $noteRepository->getCountOfFavorites($note->getId());
            };
        } catch (ApiException $e) {
            $error = 'Что-то пошло не так';
        }

        return $this->render('details.html.twig', [
            'error' => $error ?? null,
            'post' => $response ?? [],
            'favorite_amount' => $fav_amount[0]['amount'] ?? 0,
            'already_favorite' => $already_favorite ?? null
        ]);
    }


    /**
     * @Route("/profile", name="profile")
     * @param Request $request
     * @param NoteHandler $noteHandler
     * @return Response
     */
    public function profileAction(Request $request, NoteHandler $noteHandler) {
        $response = $noteHandler->sortFavoritePosts($this->getUser(), $request->get('page'));
        if ($response === false) {
            $error = 'Что-то пошло не так';
        }
        $amount = $response['amount'][0][0]['amount'];
        return $this->render('profile.html.twig', [
            'error' => $error ?? null,
            'posts' => $response['posts'] ?? [],
            'amount' => ceil(($amount/9))
        ]);
    }


    /**
     * @Route("/all/favorites", name="all-favorites")
     * @param Request $request
     * @param NoteHandler $noteHandler
     * @return Response
     */
    public function allFavoritePosts(Request $request, NoteHandler $noteHandler) {
        $response = $noteHandler->sortFavoritePosts(null, $request->get('page'));
        if ($response === false) {
            $error = 'Что-то пошло не так';
        }
        $amount = $response['amount'][0][0]['amount'];
        return $this->render('favorites.html.twig', [
            'error' => $error ?? null,
            'posts' => $response['posts'] ?? [],
            'amount' => ceil(($amount/9))
        ]);
    }

    /**
     * @Route("/favorite/add", name="favorite-add")
     * @param Request $request
     * @param ObjectManager $manager
     * @param NoteRepository $noteRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addFavoritePostAction(Request $request, ObjectManager $manager, NoteRepository $noteRepository) {
        $post_name = $request->get('post_name');
        /** @var Note $note */
        if($note = $noteRepository->checkNoteExists($post_name)) {
            if($noteRepository->isAlreadyFavorite($this->getUser(), $note)) {
                return $this->redirectToRoute('homepage');
            };
        } else {
            $note = new Note();
            $note->setName($post_name);
        }
        $user = $this->getUser();
        $user->addNote($note);
        $manager->persist($note);
        $manager->persist($user);
        $manager->flush();
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/sign-up", name="sign-up")
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function signUpAction(
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $password = $userHandler->encodePlainPassword($user->getPassword());
            $user->setPassword($password);
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('homepage');
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/sign-in", name="sign-in")
     * @param UserRepository $userRepository
     * @param Request $request
     * @return Response
     */
    public function signInAction(
        UserRepository $userRepository,
        Request $request
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm(SignInType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user_from_db = $userRepository->getByCredentials($user->getPassword(), $user->getUsername());
            if(!empty($user_from_db)){
                $token = new UsernamePasswordToken($user_from_db, null, 'main', $user_from_db->getRoles());
                $this->container->get('security.token_storage')->setToken($token);
                $this->container->get('session')->set('_security_main', serialize($token));
                return $this->redirectToRoute('homepage');
            } else {
                $error = 'Ты не тот, за кого себя выдаешь';
            }
        }

        return $this->render(
            'sign_in.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }


    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
        $this->get('security.token_storage')->setToken(null);
        $this->container->get('session')->invalidate();
        return $this->redirectToRoute('homepage');
    }
}
