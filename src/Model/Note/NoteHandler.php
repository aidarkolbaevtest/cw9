<?php

namespace App\Model\Note;


use App\Entity\Note;
use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Repository\NoteRepository;

class NoteHandler
{
    /**
     * @var ApiContext
     */
    private $apiContext;
    /**
     * @var NoteRepository
     */
    private $noteRepository;

    public function __construct(ApiContext $apiContext, NoteRepository $noteRepository)
    {
        $this->apiContext = $apiContext;
        $this->noteRepository = $noteRepository;
    }

    /**
     * @param User $user
     * @param int $page
     * @return bool
     */
    public function sortFavoritePosts(User $user = null, $page = 0) {

        $favorites = $this->noteRepository->getFavorites($user, $page - 1);
        $posts_names = [];
        /** @var Note $favorite */
        foreach ($favorites as $favorite) {
            $posts_names[] = $favorite->getName();
        }

        try {
            $reddit_response = $this->apiContext->getMyFavorites($posts_names);
            foreach ($reddit_response['data']['children'] as $post) {
                $response['posts'][] = $post['data'];
            }
            $response['amount'][] = $this->noteRepository->getCountOfFavoriteNotes($user);
            return $response ?? null;
        } catch (ApiException $e) {
            return false;
        }
    }
}