<?php

namespace App\Model\User;


use Symfony\Component\DependencyInjection\ContainerInterface;

class UserHandler
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(
        ContainerInterface $container
    )
    {
        $this->container = $container;
    }


    /**
     * @param string $password
     * @return string
     */
    public function encodePlainPassword(string $password): string
    {
        return md5($password);
    }
}
