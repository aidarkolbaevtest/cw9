<?php

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_POSTS = '/r/picture/search.json?q={phrase}&sort={sort}&limit={limit}&type=link';
    const ENDPOINT_CONCRETE_POST = '/api/info.json?id={name}';
    const ENDPOINT_FAVORITE_POSTS = '/api/info.json?id={favorites}';

    /**
     * @param string $phrase
     * @param string $sort
     * @param int $limit
     * @return mixed
     * @throws ApiException
     */
    public function getPostsByParameters(string $phrase, string $sort, int $limit)
    {
        $endPoint = $this->generateApiUrl(self::ENDPOINT_POSTS, [
            'phrase' => $phrase,
            'sort' => $sort,
            'limit' => $limit
        ]);
        return $this->makeQuery($endPoint, self::METHOD_GET);
    }


    /**
     * @param string $name
     * @return mixed
     * @throws ApiException
     */
    public function getPostByName(string $name) {
        $endPoint = $this->generateApiUrl(self::ENDPOINT_CONCRETE_POST, [
            'name' => $name
        ]);
        return $this->makeQuery($endPoint, self::METHOD_GET);
    }


    /**
     * @param array $favorites
     * @return mixed
     * @throws ApiException
     */
    public function getMyFavorites(array $favorites) {
        $endPoint = $this->generateApiUrl(self::ENDPOINT_FAVORITE_POSTS, [
            'favorites' => implode(',', $favorites)
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

}
