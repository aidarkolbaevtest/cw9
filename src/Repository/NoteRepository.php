<?php

namespace App\Repository;

use App\Entity\Note;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Note|null find($id, $lockMode = null, $lockVersion = null)
 * @method Note|null findOneBy(array $criteria, array $orderBy = null)
 * @method Note[]    findAll()
 * @method Note[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NoteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Note::class);
    }

    public function getCountOfFavorites($note_id) {
        return $this->createQueryBuilder('n')
                    ->select('COUNT(n) amount')
                    ->join('n.users', 'u')
                    ->where('n.id = :id')
                    ->setParameter('id', $note_id)
                    ->getQuery()->getResult();
    }

    public function checkNoteExists($note_name) {
        try {
            return $this->createQueryBuilder('n')
                ->where('n.name = :name')
                ->setParameter('name', $note_name)
                ->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


    public function getFavorites(User $user = null, $page = 0) {
        $qb = $this->createQueryBuilder('p')
            ->select('COUNT(u) AS HIDDEN posts', 'p')
            ->leftJoin('p.users', 'u');
        if(!empty($user)) {
            $qb->where('u = :user')->setParameter('user', $user);
        };
           return $qb->orderBy('posts', 'DESC')
            ->groupBy('p')
            ->setFirstResult(($page < 0 ? 0 : $page) * 9)
            ->setMaxResults(9)
            ->getQuery()
            ->getResult();
    }

    public function getCountOfFavoriteNotes(User $user = null) {
        $qb = $this->createQueryBuilder('n')
                ->select('COUNT(n) amount');
        if (!empty($user)) {
            $qb->leftJoin('n.users', 'u')
                ->where('u = :user')->setParameter('user', $user);
        }
        return $qb->getQuery()->getResult();
    }


    public function isAlreadyFavorite(User $user, Note $note) {
        try {
            return $this->createQueryBuilder('n')
                ->select('n')
                ->join('n.users', 'u')
                ->where('u = :user')
                ->andWhere('n = :note')
                ->setParameter('user', $user)
                ->setParameter('note', $note)
                ->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}

